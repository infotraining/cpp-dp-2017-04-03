#include "document.hpp"
#include "command.hpp"

std::string Clipboard::content = "Tekst ze schowka...";

void Document::set_memento(Document::Memento&& memento)
{
    text_ = std::move(memento.snapshot_);
}

