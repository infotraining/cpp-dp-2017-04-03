#ifndef DECORATOR_HPP_
#define DECORATOR_HPP_

#include <iostream>
#include <string>
#include <memory>

// "Component"
class Component
{
public:
	virtual void operation() = 0;    
    virtual ~Component() = default;
};


// "ConcreteComponent"
class ConcreteComponent : public Component
{
public:
    void operation() override
	{
		std::cout << "ConcreteComponent.operation()";
	}
};


// "Decorator"
class Decorator : public Component
{
protected:
    std::shared_ptr<Component> component_;
public:
    Decorator(std::shared_ptr<Component> component) : component_(component)
	{
	}
	
    void set_component(std::shared_ptr<Component> component)
	{
		component_ = component;
	}
	
    void operation() final override
	{        
        do_before_operation();
		component_->operation();
        do_after_operation();
	}
protected:
    virtual void do_before_operation() {}
    virtual void do_after_operation() {}
};


// "ConcreteDecoratorA" 
class ConcreteDecoratorA : public Decorator
{
private:
	std::string added_state_;
public:
    ConcreteDecoratorA(std::shared_ptr<Component> component) : Decorator(component)
	{
	}

protected:
    void do_after_operation() override
	{
        added_state_ = "added state";
		std::cout << " and decorated with " << added_state_;
	}
};

// "ConcreteDecoratorB"
class ConcreteDecoratorB : public Decorator
{
private:
	std::string added_behaviour()
	{
		return std::string("added behaviour");
	}
public:
    ConcreteDecoratorB(std::shared_ptr<Component> component) : Decorator(component)
	{
	}
	
    void do_after_operation() override
	{
        std::cout << " and decorated with " << added_behaviour();
	}
};

#endif /*DECORATOR_HPP_*/
