#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>
#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{
    class Text : public ShapeBase, private LegacyCode::Paragraph
    {            
    public:
        static constexpr const char* id = "Text";

        Text(int x = 0, int y = 0, const std::string& text = "");

        void draw() const override;

        std::string text() const;

        void set_text(const std::string& text);
    };
}

#endif
