#include "text_reader_writer.hpp"
#include "../text.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(make_type_index<Text>(), []{ return make_unique<TextReaderWriter>(); });
}

void Drawing::IO::TextReaderWriter::read(Drawing::Shape& shp, std::istream& in)
{
    Text& text = static_cast<Text&>(shp);

    Point pt;
    std::string str;

    in >> pt >> str;

    text.set_coord(pt);
    text.set_text(str.c_str());
}

void TextReaderWriter::write(Shape& shp, ostream& out)
{
    Text& text = static_cast<Text&>(shp);

    out << text.id << " " << text.coord() << " " << text.text() << "\n";
}
