#include "point.hpp"

using namespace std;
using namespace Drawing;

static constexpr const char opening_bracket = '[';
static constexpr const char closing_bracket = ']';
static constexpr const char separator = ',';

ostream&operator<<(ostream& out, const Point& pt)
{
    out << opening_bracket << pt.x << separator << pt.y << closing_bracket;
    return out;
}

istream&operator>>(istream& in, Point& pt)
{
    char start, separator, end;
    int x, y;

    if ( in >> start && start != opening_bracket)
    {
        in.unget();
        in.clear(ios_base::failbit);
        return in;
    }

    in >> x >> separator >> y >> end;

    if ( !in || separator != separator || end != closing_bracket )
        throw runtime_error("Stream reading error");

    pt.x = x;
    pt.y = y;

    return in;
}
