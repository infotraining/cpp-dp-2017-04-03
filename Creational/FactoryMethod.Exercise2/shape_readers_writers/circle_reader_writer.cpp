#include "circle_reader_writer.hpp"
#include "../circle.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(make_type_index<Circle>(), &make_unique<CircleReaderWriter>);
}

void CircleReaderWriter::read(Shape& shp, istream& in)
{
    Circle& circle = static_cast<Circle&>(shp);

    Point pt;
    int size;

    in >> pt >> size;

    circle.set_coord(pt);
    circle.set_size(size);
}

void CircleReaderWriter::write(Shape& shp, ostream& out)
{
    Circle& circle = static_cast<Circle&>(shp);

    out << Circle::id << " " << circle.coord() << " " << circle.size() << std::endl;
}
