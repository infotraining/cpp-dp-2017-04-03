#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.hpp"

namespace Drawing
{
    class Circle : public ShapeBase
    {
        size_t size_;
    public:
        static constexpr const char* id = "Circle";

        Circle(int x = 0, int y = 0, size_t size = 0) : ShapeBase{x, y}, size_{size}
        {}

        void draw() const override
        {
            std::cout << "Drawing a circle at " << this->coord() << " & size "
                      << size_ << std::endl;
        }
        size_t size() const;
        void set_size(const size_t& size);
    };
}

#endif // CIRCLE_H
