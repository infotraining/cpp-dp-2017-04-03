#include "circle.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;

namespace
{
    using namespace Drawing;

    bool is_registered =
            SingletonShapeFactory::instance()
                .register_creator(Circle::id, &make_unique<Circle>);
}

size_t Circle::size() const
{
    return size_;
}

void Circle::set_size(const size_t& size)
{
    size_ = size;
}
