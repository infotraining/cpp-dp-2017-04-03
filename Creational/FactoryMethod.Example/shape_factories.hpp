#ifndef SHAPE_FACTORIES_HPP
#define SHAPE_FACTORIES_HPP

#include <typeindex>
#include <functional>
#include <memory>
#include <unordered_map>
#include "shape.hpp"
#include "shape_readers_writers/shape_reader_writer.hpp"

template <typename ProductType, typename IdType = std::string, typename CreatorType = std::function<std::unique_ptr<ProductType>()>>
class Factory
{
    std::unordered_map<IdType, CreatorType> creators_;
public:
    bool register_creator(const IdType& id, CreatorType creator)
    {
        return creators_.insert(std::make_pair(id, creator)).second;
    }

    std::unique_ptr<ProductType> create(const IdType& id)
    {
        auto& creator = creators_.at(id);
        return creator();
    }
};

template <typename T>
class SingletonHolder
{
private:
    SingletonHolder() = delete;
    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;
public:
    static T& instance()
    {
        static T unique_instance;

        return unique_instance;
    }
};

using ShapeFactory =Factory<Drawing::Shape>;
using SingletonShapeFactory = SingletonHolder<ShapeFactory>;
using ShapeRWFactory = Factory<Drawing::IO::ShapeReaderWriter, std::type_index>;

template <typename T>
std::type_index make_typeindex(T& item)
{
    return std::type_index{typeid (item)};
}

template <typename T>
std::type_index make_typeindex()
{
    return std::type_index{typeid(T)};
}


#endif // SHAPE_FACTORIES_HPP
