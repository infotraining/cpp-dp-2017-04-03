# Design Patterns in C++ #

## Ankieta

* https://docs.google.com/a/infotraining.pl/forms/d/e/1FAIpQLSfJRtZDcHo-GRlZQJaIkZL0SVdpGtn6mNzd2bxCtyVHUTS_zg/viewform?hl=pl

## Dokumentacja

* [Dokumentacja](https://infotraining.bitbucket.io/cpp-dp/)

## Ustawienia proxy (VM)

Dodać na końcu pliku `.profile`:

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```